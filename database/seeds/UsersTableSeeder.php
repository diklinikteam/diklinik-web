<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class UsersTableSeeder extends Seeder{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run(){
        DB::table('users')->insert([
            'username' => 'admin',
            'name' => 'Administrator',
            'email' => 'admin@diklinik.com',
            'password' => bcrypt('admin', ['round' => 10]),
            'created_at' => \Carbon\Carbon::now()
        ]);
    }
}
