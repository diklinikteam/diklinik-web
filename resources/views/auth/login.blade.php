@extends('layouts.app')

@section('content')
<div class="row justify-content-center">
  <div class="col-md-12 my-5 text-center">
    <img src="{{ asset('img/logo.png') }}">
  </div>
  <div class="col-md-12 form-card-section py-5 justify-content-center">
    <div class="card">
      <div class="card-body">
        <form method="POST" action="{{ route('login') }}">
          @csrf
          <div class="col-md-12 text-center">
            <h4 class="font-weight-bold my-4">Silahkan Login!</h4>
          </div>
          <div class="form-group row">
            <div class="col-md-12">
              <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus placeholder="E-Mail">
              @error('email')
              <span class="invalid-feedback" role="alert">
                <strong>{{ $message }}</strong>
              </span>
              @enderror
            </div>
          </div>
          <div class="form-group row">
            <div class="col-md-12">
              <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="current-password" placeholder="Password">
              @error('password')
              <span class="invalid-feedback" role="alert">
                <strong>{{ $message }}</strong>
              </span>
              @enderror
            </div>
          </div>
          <div class="form-group row">
            <div class="col-md-6">
              <div class="form-check">
                <input class="form-check-input" type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>
                <label class="form-check-label font-weight-bold" for="remember">
                  Ingat Password
                </label>
              </div>
            </div>
            <div class="col-md-6">
              <div class="form-check text-right">
                <a href="#" class="font-weight-bold text-decoration-none" for="remember" style="color:black;">
                  Lupa Password?
                </a>
              </div>
            </div>
          </div>
          <div class="form-group row mb-0">
            <div class="col-md-12">
              <button type="submit" class="btn btn-primary w-100">
                {{ __('Login') }}
              </button>
            </div>
          </div>
          <div class="row">
            <div class="col-md-12 text-center my-4 font-weight-bold">
              <p>Belum Punya Akun? <a href="{{ url('/register') }}" class="text-white text-decoration-none">Daftar</a></p>
            </div>
          </div>
        </form>
      </div>
    </div>
  </div>
</div>
@endsection
