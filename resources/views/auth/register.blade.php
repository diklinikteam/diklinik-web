@extends('layouts.app')

@section('content')
<div class="row justify-content-center">
  <div class="col-md-12 my-5 text-center">
    <img src="{{ asset('img/logo.png') }}">
  </div>
  <div class="col-md-12 form-card-section py-5 justify-content-center">
    <div class="card">
      <div class="card-body">
        <form method="POST" action="{{ route('register') }}">
          @csrf
          <div class="col-md-12 text-center">
            <h4 class="font-weight-bold my-4">Daftarkan Akun Baru Sekarang!</h4>
          </div>
          <div class="form-group row">
            <div class="col-md-12">
              <input id="name" type="text" class="form-control @error('name') is-invalid @enderror" name="name" value="{{ old('name') }}" required autocomplete="name" autofocus placeholder="Nama">
              @error('name')
              <span class="invalid-feedback" role="alert">
                <strong>{{ $message }}</strong>
              </span>
              @enderror
            </div>
          </div>
          <div class="form-group row">
            <div class="col-md-12">
              <input id="username" type="text" class="form-control @error('username') is-invalid @enderror" name="username" value="{{ old('username') }}" required autocomplete="username" placeholder="Username">
              @error('username')
              <span class="invalid-feedback" role="alert">
                <strong>{{ $message }}</strong>
              </span>
              @enderror
            </div>
          </div>
          <div class="form-group row">
            <div class="col-md-12">
              <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" placeholder="E-Mail">
              @error('email')
              <span class="invalid-feedback" role="alert">
                <strong>{{ $message }}</strong>
              </span>
              @enderror
            </div>
          </div>
          <div class="form-group row">
            <div class="col-md-12">
              <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="new-password" placeholder="Password">
              @error('password')
              <span class="invalid-feedback" role="alert">
                <strong>{{ $message }}</strong>
              </span>
              @enderror
            </div>
          </div>
          <div class="form-group row">
            <div class="col-md-12">
              <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required autocomplete="new-password" placeholder="Confirmation Password">
            </div>
          </div>
          <div class="form-group row mb-0">
            <div class="col-md-12">
              <button type="submit" class="btn btn-primary w-100">
                {{ __('Register') }}
              </button>
            </div>
          </div>
          <div class="row">
            <div class="col-md-12 text-center my-4 font-weight-bold">
              <p>Sudah Punya Akun? <a href="{{ url('/login') }}" class="text-white text-decoration-none">Login</a></p>
            </div>
          </div>
        </form>
      </div>
    </div>
  </div>
</div>
</div>
@endsection
