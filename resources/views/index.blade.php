@extends('layouts.app')

@section('content')
<div id="carouselBanner" class="carousel slide" data-ride="carousel">
  <ol class="carousel-indicators">
    <li data-target="#carouselBanner" data-slide-to="0" class="active"></li>
    <li data-target="#carouselBanner" data-slide-to="1"></li>
    <li data-target="#carouselBanner" data-slide-to="2"></li>
  </ol>
  <div class="carousel-inner">
    <div class="carousel-caption d-none d-md-block banner-caption text-left">
      <h5 class="">Diklinik</h5>
      <p>Platform untuk memfasilitasi homecare.</p>
    </div>
    <div class="carousel-item active">
      <img src="{{ asset('img/banner/1.png') }}" class="d-block banners" alt="...">
    </div>
    <div class="carousel-item">
      <img src="{{ asset('img/banner/2.png') }}" class="d-block banners" alt="...">
    </div>
    <div class="carousel-item">
      <img src="{{ asset('img/banner/3.png') }}" class="d-block banners" alt="...">
    </div>
  </div>
  <a class="carousel-control-prev" href="#carouselBanner" role="button" data-slide="prev">
    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
    <span class="sr-only">Previous</span>
  </a>
  <a class="carousel-control-next" href="#carouselBanner" role="button" data-slide="next">
    <span class="carousel-control-next-icon" aria-hidden="true"></span>
    <span class="sr-only">Next</span>
  </a>
</div>
<div class="container">
  <div class="row my-5">
    <div class="col-sm-3">
      <div class="my-5">
        <div class="card justify-content-center align-items-center card-fitur text-center">
          <img src="{{ asset('img/icon1.png') }}" class="" style="height: 8rem; width: 8rem;">
        </div>
        <div class="card-body text-center">
          <h5 class="card-text">Konsultasi</h5>
        </div>
      </div>
    </div>
    <div class="col-sm-3">
      <div class="my-5">
        <div class="card justify-content-center align-items-center card-fitur text-center">
          <img src="{{ asset('img/icon2.png') }}" class="" style="height: 8rem; width: 8rem;">
        </div>
        <div class="card-body text-center">
          <h5 class="card-text">Home Care</h5>
        </div>
      </div>
    </div>
    <div class="col-sm-3">
      <div class="my-5">
        <div class="card justify-content-center align-items-center card-fitur text-center">
          <img src="{{ asset('img/icon3.png') }}" class="" style="height: 8rem; width: 8rem;">
        </div>
        <div class="card-body text-center">
          <h5 class="card-text">Obat</h5>
        </div>
      </div>
    </div>
    <div class="col-sm-3">
      <div class="my-5">
        <div class="card justify-content-center align-items-center card-fitur text-center">
          <img src="{{ asset('img/icon4.png') }}" class="" style="height: 8rem; width: 8rem;">
        </div>
        <div class="card-body text-center">
          <h5 class="card-text">Sewa Alat</h5>
        </div>
      </div>
    </div>
  </div>
</div>

<div class="footer mt-5 footer-landing">
  <div class="container py-5">
    <div class="row">
      <div class="col-md-6">
        <h5>Dapatkan aplikasi</h5>
        <p class="font-weight-bold align-content-center" style="font-size: 20px">Download Diklinik Sekarang Juga!</p>
        <a href="#googleplayLink">
          <img src="{{ asset('img/google_play.png') }}" class="img-fluid" style="height: 40%; width: 20%">
        </a>
      </div>
      <div class="col-md-6 text-right">
        <img src="{{ asset('img/logo.png') }}" class="img-fluid" style="height: 60%; width: 40%">
      </div>
    </div>
  </div>
</div>

@endsection
